'''
    AudioData.py
    Created by Andrew Tran, Steve Faxlanger, Siqi Luan [April 2021]
    Sponsored by General Motors, Michigan State University
    
    This file is a series of functions necessary for audio file processing in Python.
'''
import tkinter as tk        # For dialog box 
import scipy.io             # For most computational uses
import numpy as np          # For data types
import struct               

from scipy.io import wavfile
from scipy.fft import fft, fftfreq
from scipy.signal import butter, cheby1, cheby2, iirfilter, lfilter, sosfiltfilt, firwin2
from tkinter.filedialog import asksaveasfile, askopenfilename

'''
    Loads a WAV file and converts it to mono, if necessary.
    The filename will be captured by a dialog box getting the direct path of the file.
    All samples are converted into 16-bit integers as 16-bit PCM-encoded signals will have a max reponse of 96 dB.
    We reconvert it into its previous format (24-bit ints) when exporting the file out so that we preserve the original signal's integrity. 
    \returns fs The sampling rate of the waveforms.
    \returns data The numpy array the audio data is stored in.
    \returns depth The bit size of each data point in the audio signal.
'''
def LoadWAVFile():

    filename = tk.filedialog.askopenfilename(filetypes=[("WAV File", "*.wav")])
    if filename is None:
        print("Invalid file! Check the filename")
        return
    
    try:
        fs, tmp = wavfile.read(filename)
    except:
        print("Error in file, please check if WAV file is corrupted.")
        return

    f = open(filename, "rb")

    test = f.read(34) # Discard the first 34 bytes so we can read the bits sample
    precision = f.read(1)
    f.close()
  
    depth = int.from_bytes(precision, "big")
    if depth == 32:
        print("ERROR: No support for 32-bit floats.")  # At least, not yet
        return

    data = [i for i in range(len(tmp))]
    if (isinstance(tmp[0], np.ndarray)):
        print("Detected stereo sound! Please wait, the audio will be converted to mono...")
        G = (2**15)/(2**(depth-1))
        data = (G)*(tmp[:,0]/2 + tmp[:,1]/2)    # "Vectorize" the array, this speeds up conversion from stereo to mono by a significant amount
        print("Audio buffer converted from stereo to mono.  Audio quality may be degraded if both channels are out of sync.")
    else:
        print("Loading mono audio file.")
        data = tmp
        print("Done loading.")

    # Prepare the array as a specific type. 
    if depth != 16:
        data = np.asarray(data, dtype=np.int32)
    else:
        data = np.asarray(data, dtype=np.int16)

    return fs, data, depth
    
'''
    Writes the audio buffer to a WAV file.
    The audio will be written out to the same bit depth as it was taken in as, though due to limitations with SciPy's wavfile.write() function, 
    any 24-bit PCM-encoded audio signal will be converted into 32-bits instead. 
    \param buffer The audio data we want to write into a file.
    \param fs The sample rate of the audio (typically 44.1 kHz or 48 kHz)
    \param depth The bit size of each data point in the audio signal.
'''
def WriteWAVFile(buffer, fs, depth):
    filename = tk.filedialog.asksaveasfile(mode='wb', filetypes=[("WAV File","*.wav")], defaultextension=".wav")
    if filename is None:
        print("Invalid save! Check your filename")
        return

    tmpdata = ((2**(depth-1))/(2**15))*buffer.copy()

    if (depth != 16):
        newdata = np.asarray(tmpdata, dtype=np.int32)
    else:
        newdata = np.asarray(tmpdata, dtype=np.int16)
    wavfile.write(filename, fs, newdata)
    
'''
    Returns the result of the fast fourier transform to get frequency response.
    Ideally, the discrete fourier transform returns an array where each index corresponds to a frequency, however this is not true.
    To plot the actual frequency response, you need to plot the FFT data against the result of the FFTFreq function,
    where for all 0 <= i = len(FFT), every value of FFTFreq[i] is the actual frequency for every amplitude FFT[i].

    NOTE: To get the actual amplitude, you need to multiply the absolute value of every element in data_fft by (2/frames), where frames is the amount of data points in the audio file.
    We do this because the FFT is symmetric about the Y-axis, so besides dividing by the maximum number of points, we also only want half of the FFT and not the whole thing.

    \param buffer The signal we want to analyze.
    \param fs The sampling rate of the signal. 

    \returns data_fft The calculated fast fourier transform 
    \returns data_fftfreq The actual frequency at every index of data_fft (see above for explanation)
'''
def GetFFTData(buffer, fs):
    data_fft = fft(buffer)
    data_fftfreq = fftfreq(len(buffer), 1/fs)
    return data_fft, data_fftfreq

'''
    Returns the result of the fast fourier transform where each value is the true amplitude of the frequency.
    Ideally, the discrete fourier transform returns an array where each index corresponds to a frequency, however this is not true.
    To plot the actual frequency response, you need to plot the FFT data against the result of the FFTFreq function,
    where for all 0 <= i = len(FFT), every value of FFTFreq[i] is the actual frequency for every amplitude FFT[i].

    \param buffer The signal we want to analyze.
    \param fs The sampling rate of the signal. 

    \returns data_fft The calculated fast fourier transform 
    \returns data_fftfreq The actual frequency at every index of data_fft (see above for explanation)
'''
def GetFreqResponseData(buffer, fs):
    data_fft = 2*np.abs(fft(buffer)/len(buffer))
    data_fftfreq = fftfreq(len(buffer), 1/fs)
    return data_fft, data_fftfreq

'''
    Applies a butterworth bandpass filter onto audio data and filters all frequencies except what is between low and high.
    Butterworth filters are maximally flat, as in there is no ripple at the pass, stop, and transition bands.  
    The downside of butterworth filters are that the transition bands will always be 20n dB/decade, or 6.02n dB/octave, 
    so short transition bands are not possible without a higher filter order.  

    The cutoff frequencies are defined as the -3 dB point, where all frequencies at the cutoff have half power (or 1/sqrt(2) the amplitude).

    NOTE: THIS DOES NOT MEAN ANYTHING OUTSIDE OF LOW AND HIGH ARE GONE!!! There's still going to be some leakage.

    \param data The audio buffer (array of data values from audio signals)
    \param low Lower frequency of the band, determined by fc/(fc**(1/6) in Hz
    \param high Higher frequency of the band, determined by fc*(2**(1/6)) in Hz
    \param samp The sampling rate of the audio signals in Hz
    \param order The order of the filter, default to 3 

    \returns y The audio buffer 
'''
def ButterBandpassFilter(data, low, high, samp, order=3):
    nyq = 0.5*samp
    lowcut = low/nyq
    highcut = high/nyq
    # To avoid instability with narrow bands, we use "sos" (second-order sections)
    sos = butter(order, [lowcut, highcut], btype='band',analog=False,output='sos')
    y = sosfiltfilt(sos, data)
    return y

'''
    Applies a Chebyshev Type II bandpass filter onto audio data.
    Chebyshev Type II filters work by having no ripple in the passband but has some in the stopband. 
    Chebyshev filters have sharper transition bands than butterworth, 
    but the cutoff frequency is the transition band start, not the -3 dB point.  

    NOTE: Like Butterworth, THIS DOES NOT MEAN ANYTHING OUTSIDE OF LOW AND HIGH ARE GONE!!! There's still going to be some leakage.

    \param data The audio buffer (array of data values from audio signals)
    \param low Lower frequency of the band, determined by fc/(fc**(1/6)
    \param high Higher frequency of the band, determined by fc*(2**(1/6))
    \param samp The sampling rate of the audio signals 
    \param order The order of the filter, default to 2
    \returns y The audio buffer 
'''
def Cheby2BandpassFilter(data, low, high, samp, order=2):
    nyq = 0.5*samp
    lowcut = low/nyq
    highcut = high/nyq
    sos = iirfilter(N=order, Wn=[lowcut, highcut], rs=60, btype='band',ftype='cheby2', analog=False, output='sos')
    y = sosfiltfilt(sos, data)
    return y

'''
    Applies a Chebyshev Type II bandstop filter onto audio data.
    Chebyshev Type II filters work by having no ripple in the passband but has some in the stopband. 
    Chebyshev filters have sharper transition bands than butterworth, 
    but the cutoff frequency is the transition band start, not the -3 dB point.  

    NOTE: THIS DOES NOT MEAN ANYTHING OUTSIDE OF LOW AND HIGH ARE GONE!!! There's still going to be some leakage.

    \param data The audio buffer (array of data values from audio signals)
    \param low Lower frequency of the band, determined by fc/(fc**(1/6)
    \param high Higher frequency of the band, determined by fc*(2**(1/6))
    \param samp The sampling rate of the audio signals 
    \param order The order of the filter
    \returns y The audio buffer 
'''
def Cheby2BandstopFilter(data, low, high, samp, order=2):
    nyq = 0.5*samp
    lowcut = low/nyq
    highcut = high/nyq
    #b,a = iirfilter(order, [lowcut, highcut], rs=60, btype='bandstop',ftype='cheby2', analog=False)
    sos = iirfilter(N=order, Wn=[lowcut, highcut], rs=60, btype='bandstop',ftype='cheby2', analog=False, output='sos')
    y = sosfiltfilt(sos, data)
    #y = lfilter(b, a, data)
    return y

'''
    Applies a Chebyshev Type I bandpass filter onto audio data.
    Chebyshev Type I filters work by having no ripple in the stop but has some in the passband. 
    Chebyshev filters have sharper transition bands than butterworth, 
    but the cutoff frequency is the transition band start, not the -3 dB point.  

    NOTE: THIS DOES NOT MEAN ANYTHING OUTSIDE OF LOW AND HIGH ARE GONE!!! There's still going to be some leakage.

    \param data The audio buffer (array of data values from audio signals)
    \param low Lower frequency of the band, determined by fc/(fc**(1/6)
    \param high Higher frequency of the band, determined by fc*(2**(1/6))
    \param samp The sampling rate of the audio signals 
    \param order The order of the filter
    \returns y The audio buffer 
'''
def Cheby1BandpassFilter(data, low, high, samp, order=2):
    nyq = 0.5*samp
    lowcut = low/nyq
    highcut = high/nyq
    #b,a = iirfilter(order, [lowcut, highcut], rp=0.1, btype='band',ftype='cheby1', analog=False)
    sos = iirfilter(N=order, Wn=[lowcut, highcut], rp=0.1, btype='band',ftype='cheby1', analog=False, output='sos')
    y = sosfiltfilt(sos, data)
    #y = lfilter(b, a, data)
    return y

'''
    Applies a Chebyshev Type I bandstop filter onto audio data.
    Chebyshev Type II filters work by having no ripple in the stopband but has some in the passband. 
    Chebyshev filters have sharper transition bands than butterworth, 
    but the cutoff frequency is the transition band start, not the -3 dB point.  

    NOTE: THIS DOES NOT MEAN ANYTHING OUTSIDE OF LOW AND HIGH ARE GONE!!! There's still going to be some leakage.

    \param data The audio buffer (array of data values from audio signals)
    \param low Lower frequency of the band, determined by fc/(fc**(1/6)
    \param high Higher frequency of the band, determined by fc*(2**(1/6))
    \param samp The sampling rate of the audio signals 
    \param order The order of the filter
    \returns y The audio buffer 
'''
def Cheby1BandstopFilter(data, low, high, samp, order=2):
    nyq = 0.5*samp
    lowcut = low/nyq
    highcut = high/nyq
    #b,a = iirfilter(order, [lowcut, highcut], rs=60, btype='bandstop',ftype='cheby1', analog=False)
    sos = iirfilter(N=order, Wn=[lowcut, highcut], rp=0.1, btype='bandstop',ftype='cheby1', analog=False, output='sos')
    y = sosfiltfilt(sos, data)
    #y = lfilter(b, a, data)
    return y

'''
    Applies a butterworth bandstop filter onto audio data and passes all frequencies except what is between low and high.
    Butterworth filters are maximally flat, as in there is no ripple at the pass, stop, and transition bands.  
    The downside of butterworth filters are that the transition bands will always be 20n dB/decade, or 6.02n dB/octave, 
    so short transition bands are not possible without a higher filter order. 

    \param data The audio buffer (array of data values from audio signals)
    \param low Lower frequency of the band, determined by fc/(2**(1/6)
    \param high Higher frequency of the band, determined by fc*(2**(1/6))
    \param samp The sampling rate of the audio signals 
    \param order The order of the filter
    \returns y The audio buffer 
'''
def ButterBandstopFilter(data, low, high, samp, order=3):
    nyq = 0.5*samp
    lowcut = low/nyq
    highcut = high/nyq
    sos = butter(order, [lowcut, highcut], btype='stop',analog=False,output='sos')
    y = sosfiltfilt(sos, data)
    return y

'''
    Applies an FIR bandpass filter using the blackman-harris windowing method. 
    This method uses signal.firwin2 instead of signal.firwin to be concise with 
    which frequencies belong in the passband/stopband.
    FIR filters have linear phase response and little instability due to no feedback, 
    but requires more computational resources as a significantly higher order is needed to filter properly. 

    \param data The audio buffer (array of data values from audio signals)
    \param low Lower frequency of the band, determined by fc/(2**(1/6)
    \param high Higher frequency of the band, determined by fc*(2**(1/6))
    \param samp The sampling rate of the audio signals 
    \returns filteredSig The audio buffer 
'''
def FIRBandpassFilter(data, low, high, samp):
    nyq = 0.5*samp
    lowcut_zero = low/nyq
    lowcut_one  = (low+5)/nyq
    highcut_one = (high-5)/nyq
    highcut_zero  = high/nyq
    
    b = firwin2(numtaps=8192, freq=[0, lowcut_zero, lowcut_one, highcut_one, highcut_zero, 1.0], gain=[0,0,1,1,0,0], window='blackmanharris')
    filteredSig = lfilter(b, 1, data)
    return filteredSig

'''
    Applies an FIR bandstop filter using the blackman-harris windowing method. 
    This method uses signal.firwin2 instead of signal.firwin to be concise with 
    which frequencies belong in the passband/stopband.
    FIR filters have linear phase response and little instability due to no feedback, 
    but requires more computational resources as a significantly higher order is needed to filter properly. 

    \param data The audio buffer (array of data values from audio signals)
    \param low Lower frequency of the band, determined by fc/(2**(1/6)
    \param high Higher frequency of the band, determined by fc*(2**(1/6))
    \param samp The sampling rate of the audio signals 
    \returns filteredSig The audio buffer 
'''
def FIRBandstopFilter(data, low, high, samp):
    nyq = 0.5*samp
    lowcut_zero = (low-1)/nyq
    lowcut_one  = low/nyq
    highcut_one = high/nyq
    highcut_zero  = (high+1)/nyq
    nyq_cutoff = (nyq-1)/nyq
    
    b = firwin2(numtaps=8191, freq=[0, lowcut_zero, lowcut_one, highcut_one, highcut_zero, nyq_cutoff, 1.0], gain=[1,1,0,0,1,1,0], window='blackmanharris')
    filteredSig = lfilter(b, 1, data)
    return filteredSig


