'''
    Test bench code.
    This is where all of the major "system" functions go---
    all functions will be called from .py files within the current directory. 
'''
import AudioData
import scipy
import matplotlib as mpl
from matplotlib import pyplot as plt
from scipy import signal
import numpy as np
from AudioData import *
#import sounddevice as sd


#fs, data, depth = LoadWAVFile()
#fft, fftfreq = GetFreqResponseData(data, fs)
'''
plt.plot(fftfreq, fft)
plt.xlim(0, fs/2)
plt.title('FFT')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Amplitude')
#plt.grid(which='both', axis='both')
plt.show()
'''

#sd.play(data,fs)

'''
plt.plot(fftfreq, fft)
plt.xlim(0, fs/2)
plt.show()
'''
#print("Got data, applying bandpass filter between 20 and 500 Hz...")
#filtered_20500 = ButterBandpassFilter(data, 20, 500, fs)
#filtered_cheb2_20500 = Cheby2BandpassFilter(data, 20, 50, fs)
#filtered = Cheby2BandpassFilter(data, 718.4, 905.1, fs)

#buffer = [filtered[i] + filtered_cheb2_20500[i] for i in range(len(filtered))]

#freqs =        [160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000]

#print("Done.  Now applying bandstop between 20 and 500 Hz...")
#filtered_stop20500 = ButterBandstopFilter(data, 20, 500, fs)
#for i in freqs:
#    data = Cheby1BandstopFilter(data.copy(), i/2**(1/6), i*2**(1/6), fs)

#b, a = signal.butter(3, [142/(0.5*fs), 800/(0.5*fs)], btype='stop',analog=False)

#nyq = 0.5*fs
'''
print(fs)
sos = iirfilter(1, [142/(nyq), 180/(nyq)], rs=60, btype='bandstop',ftype='cheby2', analog=False, output='sos')
#sos = iirfilter(3, [142/(0.5*fs), 180/(0.5*fs)], rs=60, btype='bandstop',ftype='cheby2', analog=False, output='sos')
w, h = signal.sosfreqz(sos, worN=len(data))
plt.semilogx(w, 20 * np.log10(abs(h)))
plt.title('Butterworth bandstop filter frequency response')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Amplitude [dB]')
plt.grid(which='both', axis='both')
plt.axvline(100, color='green') # cutoff frequency

#newdata = signal.sosfiltfilt()

plt.show()
'''

#test = ButterBandpassFilter(data, 140, 180, fs)
#again = ButterBandstopFilter(data, 140, 180, fs)
#WriteWAVFile(test, fs)
#WriteWAVFile(again, fs)


#print("Done. Now exporting bandpass...")
#WriteWAVFile(filtered_20500, fs)
#WriteWAVFile(filtered_cheb2_20500, fs)

#print("Done.  Now exporting bandstop...")
#WriteWAVFile(filtered_cheb2_stop20500, fs)
#WriteWAVFile(data, fs)

#print("Exporting original audio data...")
#WriteWAVFile(data, fs)

'''
fs=48000
nyq = 0.5*fs

#b = signal.firwin2(400, [0, 140/nyq, 141/nyq, 180/nyq, 181/nyq, (nyq-1)/nyq, 1], gain=[1, 1, 0, 0, 1, 1, 0], window='blackman')

#b = signal.firwin2(8192, [0, 140/nyq, 141/nyq, 180/nyq, 181/nyq, 181/nyq, 181/nyq, 2000/nyq, 2001/nyq, 1], gain=[0, 0, 1, 1, 0, 0, 1, 1, 0, 0], window='blackmanharris')
b = signal.firwin2(2*44100, [0, 20/nyq, 139/nyq, 140/nyq, 180/nyq, 181/nyq, 250/nyq, 251/nyq, (nyq-1)/nyq, 1], gain=[0, 0, 0, 1/10, 1/10, 1, 1,0, 0, 0], window='blackmanharris')
#b = signal.firwin2(8192, [0, 140, 141, 180, 181, nyq-1, nyq], gain=[0, 0, 1, 1, 0, 0, 0], window='blackmanharris', fs=fs)
#a = 1

#c = signal.firwin2(8192, [0, 180, 181, nyq-1, nyq], gain=[1, 1, 0, 0, 0], window='blackmanharris', fs=fs)
#c, d = signal.cheby2(N=3, rs=60, Wn=15692, btype='lowpass', analog=False, fs=fs)
#c, d = signal.cheby1(N=2, rp=0.01, Wn=[120, 220], btype='bandstop', analog=False, fs=fs)
#c, d = signal.butter(N=3, Wn=4096, btype='lowpass', analog=False, fs=fs)
w, h = signal.freqz(b=b, a=1, worN=100000)
plt.plot(w*nyq/np.pi, 20 * np.log10(abs(h)))
plt.title('FIR Bandpass Frequency Response')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Amplitude [dB]')
plt.grid(which='both', axis='both')
plt.show()
#tmp = lfilter(c, d, data)
'''
#b,a = signal.butter(3, [142/nyq, 180.1/nyq], 'bandpass', analog=False)
#sos = signal.butter(5, [142/nyq, 180.1/nyq], 'bandpass', analog=False, output='sos')
#print("Done.")
#w, h = signal.freqz(b=b, a=a, worN=100000)
#w, h = signal.sosfreqz(sos, worN=fs)
#plt.plot(nyq*w/np.pi, 20 * np.log10(abs(h)))
#plt.title('FIR Bandpass Filter Frequency Response')
#plt.xlabel('Frequency [Hz]')
#plt.ylabel('Amplitude [dB]')
#plt.grid(which='both', axis='both')
#plt.show()

bands = [(142.5, 179.6), (179.6, 226.3), (226.3, 285.1)]
gains = [1, 1/10, 1/1000]
nyq = 48000/2
#gain = [0, 1]
#freqs = [0, 20/nyq] # we start at 20 since that is the lower-range of human hearing, but it is not entirely necessary
freqs = [0]
gain = [0]
print("Starting bandstop filters...")
#filtered = data.copy()
'''
for b in range(len(bands)):
    current_band = bands[b]

    freqs.append((current_band[0] - 0.5)/nyq)
    freqs.append((current_band[0] - 0)/nyq)
    freqs.append((current_band[1] - 1.1)/nyq)
    freqs.append((current_band[1] - 0.6)/nyq)

    gain.append(1)
    gain.append(0)
    gain.append(0)
    gain.append(1)

freqs.append((nyq-1)/nyq)
freqs.append(1)

gain.append(1)
gain.append(0)
'''

for b in range(len(bands)):
    current_band = bands[b]
    current_gain = gains[b]
    freqs.append(current_band[0]/nyq)
    gain.append(0)
    freqs.append((current_band[0]+0.5)/nyq)
    gain.append(current_gain)
    freqs.append((current_band[1]-0.6)/nyq)
    gain.append(current_gain)
    freqs.append((current_band[1]-0.1)/nyq)
    gain.append(0)

freqs.append(1)
gain.append(0)

b = firwin2(numtaps=10000, freq=freqs, gain=gain, window='blackmanharris')
w, h = signal.freqz(b=b, a=1, worN=100000)
plt.plot(w*nyq/np.pi, 20 * np.log10(abs(h)))
plt.title('FIR Bandstop Frequency Response')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Amplitude [dB]')
plt.grid(which='both', axis='both')
plt.show()
#filtered = lfilter(b, 1, filtered)

#result.append(filtered)