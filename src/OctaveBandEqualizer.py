'''
    Octave Band equalizer simulation tool.  Simulation of the desired tool we want to implement in C++ using JUCE.

    Project sponsor: Doug Moore

    User interface created by Arjun Balakrishnan, Shaya Master, Denny Blaschko, and Thomas Brandell.
    Audio processing created by Andrew Tran, Steven Faxlanger, and Siqi Luan.
'''
import os
import matplotlib as mpl
import scipy
import numpy as np
import tkinter as tk
import struct
import threading
import sounddevice as sd
import sys

from AudioData import *
from matplotlib import pyplot as plt
from matplotlib.widgets import Button
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

#code used as reference:
#https://stackoverflow.com/questions/9997869/interactive-plot-based-on-tkinter-and-matplotlib

'''
    The main class that contains all filtering operations, 
'''
class Plot:
    def __init__(self, root, freqs):
        self.frame = tk.Frame(master=root, width=200, height=165)
        self.frame.pack()
        self.figure1 = plt.Figure(figsize=[15,6])
        self.ax1 = self.figure1.add_subplot(111)
          
        self.freqs = freqs
        self.cols = []

        self.fileAdded = False
        self.resp_db = [0*i for i in self.freqs]
        
        # To automatically enable/disable filters,
        # we will compare if the decibel values have changed
        # if it did, the filter will be applied
        # if it didn't, the filter will not be applied
        self.originalData = []
        self.newData = []
        self.ref_resp_db = []
        self.tmpData = []
        self.tmpDataRecent = False
        self.nframerate = 0
        self.bitDepth = 16 # Assume that the input file is a standard 16-bit 

        # Gets the frequency response data 
        self.data_fft = []
        self.data_fftfreq = []

        # Gets the frequency band response 
        self.data_bands = self.octave_bands(self.middle_frequencies(self.freqs))

        self.did_bars_change = ['blue' for i in self.freqs]

        count = 1
        for item in self.freqs:
            self.cols.append('f' + str(count))
            count += 1

        self.bands = ['1\n142.5\n179.6', '2\n179.6\n226.3', '3\n226.3\n285.1',
                      '4\n285.1\n359.2', '5\n359.2\n452.5', '6\n452.5\n570.2', 
                      '7\n570.2\n718.4', '8\n718.4\n905.1', '9\n905.1\n1140.4', 
                      '10\n1140.4\n1436.8', '11\n1436.8\n1810.2', '12\n1810.2\n2280.7', 
                      '13\n2280.7\n2873.5', '14\n2873.5\n3620.4', '15\n3620.4\n4561.4', 
                      '16\n4561.4\n5747.0']
            
        self.ax1.bar(self.bands, height=self.resp_db, color = self.did_bars_change)
        plt.setp(self.ax1.get_xticklabels(), fontsize=8)
        self.ax1.set_title('Frequency Response')
        self.ax1.set_ylim(0,100)
        self.ax1.set_ylabel('dB', rotation=0, fontsize=16)
        self.ax1.set_xlabel('Hz', fontsize=16)
        self.bar1 = FigureCanvasTkAgg(self.figure1, root)
    
        self.e1 = tk.Entry(master = self.frame)
        self.e1.place(x=48,y=0)
        
        self.frequncy = None
        
        self.val = ''
        root.bind('<KeyPress-Return>', self.key_pressed)
        
        menu = tk.Menu(root)
        root.config(menu=menu)

        file = tk.Menu(menu)
        file.add_command(label="Reset Modified Bars", command = self.ResetModifiedBars)
        file.add_command(label="Clear All Data", command = self.ClearAllData)
        file.add_separator()
        file.add_command(label="Import WAV File",command = self.ImportAudioFile)
        file.add_command(label="Export WAV File",command = self.ExportAudioFile)
        
        # Import/Export under file, exit to quit client
        menu.add_cascade(label="File", menu=file)
        menu.add_command(label="Exit", command=self.exit_program)
        
        self.bar1.get_tk_widget().pack()

        self.label_num = tk.Label(text=0)
        self.label_num.place(x=720,y=30, anchor="center")
        
        self.info_label1 = tk.Label(text='Enter bar number (for multiple bars, enter numbers seperated by commas):')
        self.info_label1.place(x=460,y=10, anchor="center")
        
        self.info_label2 = tk.Label(text='Enter desired bar value:')
        self.info_label2.place(x=590,y=120, anchor="center")
        
        self.button1 = tk.Button(text="increase", fg="blue", command=self.increase)
        self.button1.place(x=720,y=60, anchor="center")
        self.button2 = tk.Button(text="decrease", fg="blue", command=self.decrease)
        self.button2.place(x=720,y=85, anchor="center")  
        
        self.e2 = tk.Entry(master = self.frame)
        self.e2.place(x=100,y=120, anchor="center")
        
        self.button3 = tk.Button(text="Set value", fg="blue", command=self.set)
        self.button3.place(x=657,y=135) 
        
        self.button3 = tk.Button(text="Set to zero", fg="blue", command=self.zero)
        self.button3.place(x=718,y=135) 
        
        self.button4 = tk.Button(text="Play original sound", fg="blue", command=self.PlayOriginalFile)
        self.button4.place(x=1000,y=54, anchor="center") 
        
        self.button5 = tk.Button(text="Play modified sound", fg="blue", command=self.PlayModifiedFile)
        self.button5.place(x=1000,y=80, anchor="center") 
    
        self.button6 = tk.Button(text="Stop playing sound", fg="blue", command=lambda : sd.stop())
        self.button6.place(x=1000,y=106, anchor="center") 

        self.flag = None

    # Defines function to set one or multiple bands to one dB value. 
    def set(self):
        if self.fileAdded is False:
            print("Please import a WAV file.")
        else:
            if self.e2.get().isdigit() is False:
                return
            val = int(self.e2.get())
            if self.flag != 'invalid':
                if self.flag is False:
                    idx = self.frequncy - 1
                    self.resp_db[idx] = val
                    if val >= 80:
                        self.did_bars_change[idx] = 'red'
                    else:
                        self.did_bars_change[idx] = 'turquoise'
                    self.label_num["text"] = str(val)
                else:
                    for item in self.frequncy_list:
                        idx = item - 1
                        self.resp_db[idx] = val
                        if val >= 80:
                            self.did_bars_change[idx] = 'red'
                        else:
                            self.did_bars_change[idx] = 'turquoise'
                    word_str = ''
                    for item in self.frequncy_list:
                        word_str += str(val) + ', '
                    word_str = word_str[0:-2]
                    self.label_num["text"] = word_str
                self.Update()
        
    # Defines function to set one or multiple bands to zero dB.
    def zero(self):
        if self.fileAdded is False:
            print("Please import a WAV file.")
        else:
            if self.flag != 'invalid':
                if self.flag is False:
                    idx = self.frequncy - 1
                    self.resp_db[idx] = 0
                    self.did_bars_change[idx] = 'turquoise'
                    self.label_num["text"] = str(0)
                else:
                    for item in self.frequncy_list:
                        idx = item - 1
                        self.resp_db[idx] = 0
                        self.did_bars_change[idx] = 'turquoise'
                    word_str = ''
                    for item in self.frequncy_list:
                        word_str += str(0) + ', '
                    word_str = word_str[0:-2]
                    self.label_num["text"] = word_str
                self.Update()

    # Checks if the input value for which band to manipualte is valid.
    def check_input(self, word):
        if ',' in word:
            word = word.split(',')
        else:
            word = [word]
        for item in word:
            if item.isdigit() is False:
                self.flag = 'invalid'
                self.label_num["text"] = "not valid"
                self.button1["text"] = "increase"
                self.button2["text"] = "decrease"
                return
            else:
                if int(item)<1 or int(item)>16:
                    self.flag = 'invalid'
                    self.label_num["text"] = "not valid"
                    self.button1["text"] = "increase"
                    self.button2["text"] = "decrease"
                    return
        if ',' in word:
            self.flag = True
        else:
            self.flag = False

    def key_pressed(self, event, val = 'hi'):
        word = self.e1.get()
        self.check_input(word)
        if self.flag != 'invalid':
            if ',' in word:
                self.flag = True
                word = word.split(',')
                count = 0
                for item in word:
                    word[count] = int(item)
                    count += 1
                word_str = ''
                for item in word:
                    word_str += str(self.resp_db[item-1]) + ', '
                word_str = word_str[0:-2]
                
                self.label_num["text"] = word_str

                self.frequncy_list = word
                
                self.button1["text"] = "increase " + str(word)
                self.button2["text"] = "decrease " + str(word)
                
                
            else:
                word = int(self.e1.get())
                self.flag = False
                self.label_num["text"] = self.resp_db[word-1]
                self.frequncy = word
                if 0 < self.frequncy <= 16:
                    self.label_num["text"] = self.resp_db[word-1]
                    self.button1["text"] = "increase " + str(word)
                    self.button2["text"] = "decrease " + str(word)
            
    def increase(self):
        if self.fileAdded is False:
            print("Please import a WAV file.")
        else:
            if self.flag is False:
                idx = self.frequncy - 1
                if self.resp_db[idx] < 80:
                    self.did_bars_change[idx] = 'turquoise'
                else:
                    self.did_bars_change[idx] = 'red'
                self.resp_db[idx] = self.resp_db[idx] + 1
                
                self.label_num["text"] = str(self.resp_db[idx])
            if self.flag is True:
                for item in self.frequncy_list:
                    idx = item - 1
                    if self.resp_db[idx] <= 80:
                        self.did_bars_change[idx] = 'turquoise'
                    else:
                        self.did_bars_change[idx] = 'red'
                    self.resp_db[idx] = self.resp_db[idx] + 1
                    
                word_str = ''
                for item in self.frequncy_list:
                    word_str += str(self.freqs[item-1]) + ', '
                word_str = word_str[0:-2]
                self.label_num["text"] = word_str 
            self.Update()

            

        '''
        idx = self.cols.index(self.frequncy)
        value = self.resp_db[idx]
        self.label_num["text"] = f"{value + 1.00}"

        if self.fileAdded == True:
            self.resp_db[idx] = self.resp_db[idx] + 1.0
            if self.resp_db[idx] <= 80:
                self.did_bars_change[idx] = 'turquoise' 
            else:
                print("WARNING: Going higher than 80 dB will damage hearing.") 
                self.did_bars_change[idx] = 'red' 
        else:
            print("No sound file was added!  Please import a WAV file")
        '''
        
        
    def decrease(self):
        if self.fileAdded is False:
            print("Please import a WAV file.")
        else:
            if self.flag is False:
                idx = self.frequncy - 1
                if self.resp_db[idx] < 80:
                    self.did_bars_change[idx] = 'turquoise'
                else:
                    self.did_bars_change[idx] = 'red'
                self.resp_db[idx] = self.resp_db[idx] - 1
                
                self.label_num["text"] = str(self.resp_db[idx])
            if self.flag is True:
                for item in self.frequncy_list:
                    idx = item - 1
                    if self.resp_db[idx] <= 80:
                        self.did_bars_change[idx] = 'turquoise'
                    else:
                        self.did_bars_change[idx] = 'red'
                    self.resp_db[idx] = self.resp_db[idx] - 1
                    
                word_str = ''
                for item in self.frequncy_list:
                    word_str += str(self.freqs[item-1]) + ', '
                word_str = word_str[0:-2]
                self.label_num["text"] = word_str 
            self.Update()

        '''
        idx = self.cols.index(self.frequncy)
        value = self.resp_db[idx]
        self.label_num["text"] = f"{value- 1.00}"
        
        if self.fileAdded == True:
            self.resp_db[idx] = self.resp_db[idx] - 1.0
            self.did_bars_change[idx] = 'turquoise'
        else:
            print("No sound file was added!  Please import a WAV file.")
        self.Update()
        '''

    '''
        Redraws the graph to display changes in the UI.
    '''
    def Update(self):
        self.bar1.get_tk_widget().pack_forget()
        self.ax1.clear()
        self.ax1.bar(self.bands, height=self.resp_db, color = self.did_bars_change)
        plt.setp(self.ax1.get_xticklabels(), fontsize=8)
        self.ax1.set_title('Frequencies')
        self.ax1.set_ylim(0, 100)
        self.ax1.set_ylabel('dB', rotation=0, fontsize=16)
        self.ax1.set_xlabel('Hz', fontsize=16)
        self.bar1 = FigureCanvasTkAgg(self.figure1, root)
        self.bar1.get_tk_widget().pack()
        self.tmpDataRecent = False

    '''
        Gets the necessary data from the audio file and imports it to NumPy arrays.
    '''
    def ImportAudioFile(self):
        print("Importing file...")

        try:
            self.nframerate, self.originalData, self.bitDepth = LoadWAVFile()
        except:
            print("You may have cancelled the load operation.  Please try again.")
            return

        # Clear the data we want to work with.
        self.newData = []
        self.data_fft = []
        self.data_fftfreq = []
        self.fft_band_resp = []
        self.tmpData = []
        self.tmpDataRecent = False
        
        print("Imported file.  Processing frequency response...")
        self.data_fft, self.data_fftfreq = GetFreqResponseData(self.originalData, self.nframerate)
        print("Done. ")

        for i in range(len(self.data_bands)):
            tmp = self.data_bands[i]
            self.resp_db[i] = self.calcBandResponse(tmp[0], tmp[1])
            self.did_bars_change[i] = 'blue'
        
        self.ref_resp_db = self.resp_db.copy()

        # Sets a flag to determine if the import was successful.
        self.fileAdded = True
        self.Update()
        
    '''
        Calculates the frequency response of each 1/3 octave band.
    '''
    def calcBandResponse(self, low, up):
        freqResp = 0.0
        totalResp = 0.0
        freqCnt = 0
        maxResp = 0.0
        n = 0
        for i in self.data_fftfreq:
            if i >= low:
                if i < up:  
                    if self.data_fft[n] > maxResp:
                        maxResp = self.data_fft[n]
                else:
                    break
            n = n + 1
        
        if maxResp < 1.0: 
            maxResp = maxResp + 1.0

        freqResp = 20*np.log10(maxResp)
        return freqResp

    '''
        Applies the filters and exports the resulting audio to the specified location.
    '''
    def ExportAudioFile(self):
        if len(self.originalData) == 0:
            print("No audio data!")
            return

        print("Exporting audio file...")
        if self.tmpDataRecent is False:
            self.tmpData = self.ApplyFilters()
            self.tmpDataRecent = True

        WriteWAVFile(self.tmpData, self.nframerate, self.bitDepth)
        print("Done.")

    def PlayOriginalFile(self):
        if (len(self.originalData) == 0):
            print("No audio data!")
            return

        sd.stop()
        print("Playing original audio...")
        tmpSound = ((2**self.bitDepth)/(2**16))*self.originalData.copy()
        if self.bitDepth != 16:
            tmpSound = np.asarray(tmpSound, dtype=np.int32)
        else:
            tmpSound = np.asarray(tmpSound, dtype=np.int16)

        sd.play(tmpSound, self.nframerate)

    def PlayModifiedFile(self):
        if (len(self.originalData) == 0):
            print("No audio data!")
            return
        sd.stop()
        if self.tmpDataRecent is False:
            print("Please wait, applying filters...")
            self.tmpData = self.ApplyFilters()
            self.tmpData = self.tmpData*( (2**(self.bitDepth -1 ))/(2**15) )
            self.tmpDataRecent = True
            print("Done applying filters.")
        
        if self.bitDepth != 16:
            self.tmpData = np.asarray(self.tmpData, dtype=np.int32)
        else:
            self.tmpData = np.asarray(self.tmpData, dtype=np.int16)
        
        print("Playing modified audio...")
        sd.play(self.tmpData, self.nframerate)

    def ApplyFilterThread(self, gains, bands, data, result):
        nyq = self.nframerate/2
        gain = [0]
        freqs = [0]
        filtered = data.copy()

        print("Starting bandpass filters...")
        for b in range(len(bands)):
            current_band = bands[b]
            current_gain = gains[b]
            freqs.append(current_band[0]/nyq)
            gain.append(0)
            freqs.append((current_band[0]+1)/nyq)
            gain.append(current_gain)
            freqs.append((current_band[1]-2)/nyq)
            gain.append(current_gain)
            freqs.append((current_band[1]-1)/nyq)
            gain.append(0)

        freqs.append(1)
        gain.append(0)

        b = firwin2(numtaps=10000, freq=freqs, gain=gain, window='blackmanharris')
        filtered = lfilter(b, 1, filtered)
        result.append(filtered)
        print("Done with passband filter.")

    def GetBandstopFiltered(self, bands, data, result):
        nyq = self.nframerate/2
        gain = [0, 1]
        freqs = [0, 20/nyq] # we start at 20 since that is the lower-range of human hearing, but it is not entirely necessary
        print("Starting bandstop filters...")
        filtered = data.copy()
        for b in range(len(bands)):
            current_band = bands[b]
    
            freqs.append((current_band[0] - 0.5)/nyq)
            freqs.append((current_band[0] - 0)/nyq)
            freqs.append((current_band[1] - 1.1)/nyq)
            freqs.append((current_band[1] - 0.6)/nyq)

            gain.append(1)
            gain.append(0)
            gain.append(0)
            gain.append(1)
        
        freqs.append((nyq-1)/nyq)
        freqs.append(1)

        gain.append(1)
        gain.append(0)

        b = firwin2(numtaps=10000, freq=freqs, gain=gain, window='blackmanharris')
        filtered = lfilter(b, 1, filtered)

        result.append(filtered)

        print("Done with bandstop filter.")

    '''
        Defines the function to apply the bandpass/bandstop filters.
        The filters are only applied to the bands that were modified in the UI.  
        Uses multithreading to calculate both bandpass and bandstop filter response to speed up program time.  
    '''
    def ApplyFilters(self):
        checkIfFilterHappened = False
        preampGain = float(10**float(-6/20))
        mAudioData = preampGain*self.originalData.copy()
        buffer = [0]*mAudioData.copy()    # Buffer will contained modified signal
        passed = []
        passedSigs = []
        gainVals = []
        stoppedSigs = []
        threads = []
        results = []

        for i in range(len(self.did_bars_change)):
            data_band = self.data_bands[i]
            color = self.did_bars_change[i]

            diff = self.resp_db[i] - self.ref_resp_db[i]
            gain = float(10**float(diff/20))
            
            if color != 'blue':
                checkIfFilterHappened = True
                passedSigs.append(data_band)
                stoppedSigs.append(data_band)
                gainVals.append(gain)
 
        if checkIfFilterHappened == False:
            print("No bands were modified, returning original data.")
            return self.originalData

        print("Modifying frequency bands that have changed...")
        passThread = threading.Thread(target=self.ApplyFilterThread, args=(gainVals, passedSigs, mAudioData, results))
        stopThread = threading.Thread(target=self.GetBandstopFiltered, args=(stoppedSigs, mAudioData, results))
        threads.append(passThread)
        threads.append(stopThread)
        passThread.start()
        stopThread.start()
        
        for thread in threads:
            thread.join()
        
        for r in results:
            buffer = r + buffer
        
        print("Done filtering.")
        postampGain = float(10**float(6/20))
        buffer = postampGain*buffer
        return buffer
                
    def middle_frequencies(self, freq):
        mf_list = []
        stop = freq[-1]
        fo = freq[0] / 2**(1/3)
        while fo <= stop:
            fo = fo * 2**(1/3)
            mf_list.append(fo)
        return mf_list

    def octave_bands(self, mf):
        bands_list=[]
        for i in mf:
            ub = round(i*2**(1/6),1)
            lb = round(i/2**(1/6),1)
            bands = (lb,ub)
            bands_list.append(bands)
        return bands_list

    '''
        Resets any modifications made to the data and the UI.
        The function only resets if a valid file was loaded into the program. 
    '''
    def ResetModifiedBars(self):
        if self.nframerate != 0:
            self.resp_db = self.ref_resp_db.copy()
            self.did_bars_change = ['blue' for i in self.did_bars_change]
            self.tmpData = []
            self.tmpDataRecent = []
            self.Update()
        else:
            print("No data was loaded, so there is no reset occuring.")

    '''
        Clears all data from the program.
        If no data was loaded into the program before, nothing will happen.
    '''
    def ClearAllData(self):
        if self.nframerate == 0:
            print("No data loaded, therefore no data cleared.")
            return 

        self.newData = []
        self.originalData = []
        self.data_fft = []
        self.data_fftfreq = []
        self.fft_band_resp = []
        self.tmpData = []
        self.tmpDataRecent = False

        self.resp_db = [i*0 for i in self.resp_db]
        self.ref_resp_db = self.resp_db.copy()

        self.did_bars_change = ['blue' for i in self.did_bars_change]

        self.nframerate = 0
        self.Update()
        print("Cleared all data.")
        

    '''
        Exits the program.
        In a regular IDE/terminal shell, using quit() or exit() would suffice.
        However, when using the executable from PyInstaller, those two are
        invalid, so we must destroy the tkinter object and exit via sys.exit()
    '''
    def exit_program(self):
        self.frame.destroy()
        sys.exit()


freqs =        [160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000]
root= tk.Tk() 
root.title("1/3 Octave Band Equalizer")
graph = Plot(root, freqs)
root.mainloop()
