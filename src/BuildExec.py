'''
    This script generates the executable of the Python program. 

'''
import subprocess
import sys
from sys import platform

if platform == "win32":
    subprocess.call(r'python -m pip install -r requirements.txt')
    subprocess.call(r'python -m PyInstaller --onefile OctaveBandEqualizer.py --name OctaveBandEqualizer')
else:
    subprocess.call(r'pip install -r requirements.txt')
    subprocess.call(r'pyinstaller --onefile OctaveBandEqualizer.py --name OctaveBandEqualizer')