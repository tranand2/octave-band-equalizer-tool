import tkinter as tk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

#code used as reference:
#https://stackoverflow.com/questions/9997869/interactive-plot-based-on-tkinter-and-matplotlib

class Plot:
    def __init__(self, root, freqs):
        self.frame = tk.Frame(master=root, width=200, height=115)
        self.frame.pack()
        self.figure1 = plt.Figure()
        self.ax1 = self.figure1.add_subplot(111)
          
        self.freqs = freqs
        self.cols = []
        
        count = 1
        for item in self.freqs:
            self.cols.append('f' + str(count))
            count += 1
            
        self.ax1.bar(self.cols, self.freqs, color = 'blue')
        self.ax1.set_title('Frequencies')
        self.ax1.set_ylim(0,5000)
        self.bar1 = FigureCanvasTkAgg(self.figure1, root)
    
        self.e1 = tk.Entry(master = self.frame)
        self.e1.place(x=48,y=0)
        
        self.frequncy = None
        
        self.val = ''
        root.bind('<KeyPress-Return>', self.key_pressed)
            
        self.bar1.get_tk_widget().pack()
        
    def key_pressed(self, event, val = 'hi'):
        
        word = self.e1.get()
        try:
            if self.label.winfo_ismapped():
                self.label.pack_forget()
        except:
            pass

        try:
            if self.label_num.winfo_ismapped():
                pass
        except:
            pass
            self.label_num = tk.Label(text=self.freqs[self.cols.index(self.e1.get())])
            self.label_num.place(x=205,y=30)
        self.frequncy = self.e1.get()
        if word in self.cols:
            try:
                if self.button1.winfo_ismapped():
                    self.button1["text"] = "increase " + self.e1.get()
                    self.button2["text"] = "decrease " + self.e1.get()
                    self.label_num["text"] = self.freqs[self.cols.index(self.e1.get())]
            except:

                self.button1 = tk.Button(text="increase " + self.e1.get(), fg="blue", command=self.increase)
                self.button1.place(x=185,y=60)
                self.button2 = tk.Button(text="decrease " + self.e1.get(), fg="blue", command=self.decrease)
                self.button2.place(x=185,y=85)
            
            
        else:
            self.label = tk.Label(text="not valid")
            self.label.pack()
            
    def increase(self):
        value = int(self.label_num["text"])
        self.label_num["text"] = f"{value + 100}"
        idx = self.cols.index(self.frequncy)
        self.freqs[idx] = self.freqs[idx] + 100
        self.bar1.get_tk_widget().pack_forget()
        self.ax1.bar(self.cols, self.freqs, color = 'blue')
        self.ax1.set_title('Frequencies')
        self.ax1.set_ylim(0,5000)
        self.bar1 = FigureCanvasTkAgg(self.figure1, root)
        self.bar1.get_tk_widget().pack()
        
        
    def decrease(self):
        value = int(self.label_num["text"])
        self.label_num["text"] = f"{value- 100}"
        idx = self.cols.index(self.frequncy)
        self.freqs[idx] = self.freqs[idx] - 100
        self.bar1.get_tk_widget().pack_forget()
        self.ax1.clear()
        self.ax1.bar(self.cols, self.freqs, color = 'blue')
        self.ax1.set_title('Frequencies')
        self.ax1.set_ylim(0,5000)
        self.bar1 = FigureCanvasTkAgg(self.figure1, root)
        self.bar1.get_tk_widget().pack()     


freqs = [3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000]
root= tk.Tk() 
graph = Plot(root, freqs)
root.mainloop()
