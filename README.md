# Introduction
This is a 1/3 octave band equalizer tool for General Motors.  As part of the ECE 480/ME 481 capstone, this tool is intended to display 1/3 octave band data from an input .wav file, manipulate the audio via an equalizer, and then output a .wav file with the appropriate modifications.  This is intended to be a Logic Pro plugin, however since not everyone has Macs nor Logic Pro, this provides the structure that we can soon implement in Logic Pro.  There is no expectation for real-time signal processing currently.  

Thsi program was tested using the latest verison of Windows 10.  We do not expect cross-compatibility issues with Linux/Mac/Windows.

# How to run this software
Using git, you can collaborate with others and get/make up-to-date changes without needing to zip the deliverables and storing them in places like OneDrive.  Most IDE's have Git build-in or as an add-on that will aid visually, however here are the instructions to do so via Git CLI.
1. Download Git from the official website: https://git-scm.com/downloads
2. Create the directory where you want to access the source code
3. Open Git and get to that directory using "cd" to change current position and "ls" to list folders/files in current position
4. Run "git init" to create an empty git repository
5. Sync the directory to the git repo by typing "git remote add origin https://gitlab.msu.edu/tranand2/octave-band-equalizer-tool.git" 
6. Run "git pull" to download the latest version and updates in the master/main branch

## Operation
### Main
On startup, a user interface with an empty graph is displayed.  Users can begin manipulating audio by selecting "Import WAV File" under File, where after processing the graph will change to display the frequency response of each 1/3 octave band. 

From there, the top text box is where the desired frequency bands can be selected.  Type in a number corresponding to the band(s) on the bottom of the graph and press the Enter key when finished.  The buttons labeled "Increase" and "Decrease" will increment and decrement the dB values of those bands by 1.  The bottom text box is where we can set the desired dB response of the selected bands, where we can select "Set value" to change all selected bands to the value specified in the text box or "Set to zero" to set the bands to zero dB.  

Buttons on the right side are used to play the original and modified sounds, as well as stopping it.  The "Play original sound" button will play the imported file with no modifications, while the "Play modified sound" button will either play the audio with the filters applied or the original sound, depending on whether any modifications were made in the UI.  


### Testing
A sound folder is provided for testing the program.  If this program works as expected, then it should display the correct frequency data for all .wav files labeled as such.  Users are encouraged to stress-test this program using music, personal recordings, podcasts, etc.  Basically anything that has many frequencies and/or long.  To prevent copyright issues, no sample music will be provided.  

Those files are not provided by GM and is not intended to be vehicle sounds from them---they are only sine waves to test basic functionality.  

### Sound File Collection
While data collection will vary, there are ways on obtaining .wav files/reading audio metadata such as sampling rate, bitrate, encoding, etc.  In Linux, audio metadata can be found via the program "soxi":
```
soxi sound/600hz.wav
```
Similarly, youtube-dl works to get the .wav file from any youtube video:
```
youtube-dl --extract-audio --audio-format wav <youtube link>
```


## Prerequisites
### Required Python version
* Python 3.7 64-bit

### Required libraries
* numpy
* scipy
* matplotlib
* tkinter
* sounddevice

## Instructions
The required libraries can be installed via pip, python's own library management program.
1. Install the latest python version from https://www.python.org/
2. Run "pip install -r requirements.txt" to install the necessary libs
3. Run main.py in src/ with python3 

## Language Considerations
Python was selected for its feature-rich libraries, however if need be, we will consider implementing this in other languages like C/C++ for performance.  
